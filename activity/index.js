const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')

txtFirstName.addEventListener('keyup', () => { 
	spanFullName.innerHTML = txtFirstName.value
}); 

txtLastName.addEventListener('keyup', () => { 
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value} `
}) ;
