// querySelector() method returns the first element that matches a selector

const textFirstName = document.querySelector('#text-first-name');
const spanFullName = document.querySelector('#span-full-name');

// addEventListener() - method attaches an event handler to an element
// keyup - A keyboard key is released after being pushed
textFirstName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = textFirstName.value;
});

textFirstName.addEventListener('keyup', (event) => {
    console.log(event.target);
    console.log(event.target.value)
})